# README #

## Cloudy With A Chance Of - Tested on Galaxy Nexus 4.2.1

### Overview: ###

* App with single activity with frame layouts for main screen and settings screen.
* Features include: animation, persistence using shared preferences, user location and a simple widget.

### App: ###
**Home View**

* Uses three images in frame layout for an animated background.
* Contains a container taking up 20% of the screen which displays the weather icon, temperature, location (city) and weather condition.
* Weather can be updated through either pressing anywhere in the container or on the refresh button in the top menu.
* A white spinner is displayed at the center of the container when an http request is made.


**Settings Screen** 

*Accessed through settings button on top menu. Only way to exit is by pressing done. Pressing done uses the entered data to compose the http request and fetches the weather data.*

Contains:

* A textfield to enter a user id.
* Radio buttons to switch temperature units (celsius and fahrenheit).
* Radio buttons to toggle between default(provided) location and user's current location.
* User's current location is fetched from the mobile network operator.
* User id and location are persisted only on clicking the done button.
* Location is also persisted when current location option is selected and the current location changes.



### Widget: ###
Simple light weight widget which displays:

* A time field to show that updates are happening every 30 seconds (30 seconds is too frequent but can easily be changed).
* Current temperature, city and conditions for the persisted user id and location.
* Pressing anywhere on the widget opens up the app.

*The widget only shows temperature in Fahrenheit for now.*