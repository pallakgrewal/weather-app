package com.example.yuki;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
 
public class ExampleAppWidgetProvider extends AppWidgetProvider {
	
	 @Override
	 public void onDeleted(Context context, int[] appWidgetIds) {
	  Toast.makeText(context, "TimeWidgetRemoved id(s):"+appWidgetIds, Toast.LENGTH_SHORT).show();
	  super.onDeleted(context, appWidgetIds);
	 }
	 
	 @Override
	 public void onDisabled(Context context) {
	  Toast.makeText(context, "onDisabled():last widget instance removed", Toast.LENGTH_SHORT).show();
	  Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
	  PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
	  AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	  alarmManager.cancel(sender);
	  super.onDisabled(context);
	 }
	 
	 @Override
	 public void onEnabled(Context context) {
	  super.onEnabled(context);
	  AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
	  Intent intent = new Intent(context, AlarmManagerBroadcastReceiver.class);
	  PendingIntent pi = PendingIntent.getBroadcast(context, 0, intent, 0);
	  //After 30 seconds
	  am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+ 1000 * 3, 30000 , pi);
	 }
	 
	 @Override
	 public void onUpdate(Context context, AppWidgetManager appWidgetManager,
	   int[] appWidgetIds) {
		 Log.w("Widget provider", "Update called");
	  ComponentName thisWidget = new ComponentName(context,
	    ExampleAppWidgetProvider.class);
	  for (int widgetId : appWidgetManager.getAppWidgetIds(thisWidget)) {
		  // Create an Intent to launch ExampleActivity
	     Intent intent = new Intent(context, com.example.yuki.MainActivity.class);
	     PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

	     // Get the layout for the App Widget and attach an on-click listener
	     // to the button
		 RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget1);
		 remoteViews.setOnClickPendingIntent(R.id.widgetButton, pendingIntent);
		 
	     appWidgetManager.updateAppWidget(widgetId, remoteViews);
	  }
	 }
}