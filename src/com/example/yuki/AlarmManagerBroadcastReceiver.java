package com.example.yuki;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.PowerManager;
import android.util.Log;
import android.widget.RemoteViews;

public class AlarmManagerBroadcastReceiver extends BroadcastReceiver {
	// Persistence
	public static final String PREFS_NAME = "MyPrefsFile";
	SharedPreferences myPrefs;
	private String currUserId;
	private String currLatitude;
	private String currLongitude;
	
	private String currTemp;
	private String currCondition;
	private String currLocation;
	private RequestQueue queue;
	private Context currContext;

	private final String SYMBOL_FAHRENHEIT = "\u2109"; 

	public void onReceive(Context context, Intent intent) {
	Log.w("alarm manager", "receive called");
	currContext = context;
	
	PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "TAG");
	
	//Acquire the lock
	wl.acquire();
		//You can do the processing here update the widget/remote views.
		updateWeather();
	//Release the lock
	wl.release();
	}

	//================================================================================
    // Weather update methods (taken from main activity)
    //================================================================================
	private  void parseJSON(JSONObject json) throws JSONException{ 
        JSONObject platformResponse = json.getJSONObject("platformResponse");
        JSONObject weather = platformResponse.getJSONObject("weather");
        JSONObject currentCondition = weather.getJSONObject("currentCondition");
        
        // get current location
        JSONObject locationData = currentCondition.getJSONObject("locationData");
    	currLocation = locationData.getString("city");
        
		// get current temperature
        JSONObject weatherData = currentCondition.getJSONObject("weatherData");
        JSONObject temperatureData = weatherData.getJSONObject("temperatureData");
		currTemp = temperatureData.getString("tempFahrenheit") + SYMBOL_FAHRENHEIT;
        
    	
        // get current condition
		currCondition = currentCondition.getString("currCondition");
    	
		Format formatter = new SimpleDateFormat("hh:mm:ss a");
		String lastUpdated = formatter.format(new Date()).toString();
		
		RemoteViews remoteViews = new RemoteViews(currContext.getPackageName(), R.layout.widget1);
		remoteViews.setTextViewText(R.id.widgetTimer, lastUpdated);
		remoteViews.setTextViewText(R.id.widgetTemperature, currTemp);
		remoteViews.setTextViewText(R.id.widgetLocation, currLocation);
		remoteViews.setTextViewText(R.id.widgetCondition, currCondition);
		ComponentName thiswidget = new ComponentName(currContext, ExampleAppWidgetProvider.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(currContext);
		manager.updateAppWidget(thiswidget, remoteViews);
	}
		 
	public void updateWeather() {
		// fetch data
		queue = VolleySingleton.getInstance(currContext).getRequestQueue();
		
    	myPrefs = currContext.getSharedPreferences("myPrefs", Context.MODE_PRIVATE);
    	String savedUserId = myPrefs.getString("USERID",null);
    	String savedLatitude = myPrefs.getString("LATITUDE",null);
    	String savedLongitude = myPrefs.getString("LONGITUDE",null);
    	
    	if (savedUserId != null){
        	currUserId = savedUserId;
    	} else {
        	currUserId = "";
    	}

    	if (savedLatitude != null)
    	{
        	currLatitude = savedLatitude;
    	} else {
        	currLatitude = "39.923614";    		
    	}
    	
    	if (savedLongitude != null)
    	{
          	currLongitude = savedLongitude;
    	} else {
          	currLongitude = "116.396086";
    	}
		
		String url = "http://ec2-54-198-118-231.compute-1.amazonaws.com:8080/wunderground/getWeather?input=%7B%22gps%22:%7B%22latitude%22:"+currLatitude+",%22longitude%22:"+currLongitude+",%22radius%22:0.0%7D%7D&oauthToken=ANDROIDINTERVIEW&requestInfo=%7B%22userId%22:%22user-1234%22%7D";
	
		JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
	        @Override
	        public void onResponse(JSONObject response) {
	            try {
					parseJSON(response);
				} catch (JSONException e) {
					Log.w("JSON object request", "Fail!!!");
						
						e.printStackTrace();
					}
	            }
	        }, new Response.ErrorListener() {
	 
	            @Override
	            public void onErrorResponse(VolleyError error) {
					Log.w("VolleyError", "Fail!!!");
	            }
	        });
	 
	        queue.add(jsObjRequest);
	}
}
