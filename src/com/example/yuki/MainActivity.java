package com.example.yuki;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;

public class MainActivity extends ActionBarActivity {
	// Persistence
	public static final String PREFS_NAME = "MyPrefsFile";
	SharedPreferences myPrefs;
	
	// Temperature constants
	private final String SYMBOL_CELSIUS = "\u2103"; 
	private final String SYMBOL_FAHRENHEIT = "\u2109"; 
	
	// Location constants
    private static final long MINIMUM_DISTANCE_CHANGE_FOR_UPDATES = 1; // in Meters
    private static final long MINIMUM_TIME_BETWEEN_UPDATES = 1800000; // in Milliseconds
	
    // Background view
	ImageView imageView = null;
	ImageView imageView2 = null;
	ImageView imageView3 = null;
	
    // Request and display parameters    
	private RequestQueue queue;
	private ImageLoader mImageLoader;
	private TextView currTempDisplay;
	private TextView currConditionDisplay;
	private TextView currLocationDisplay;
	private TextView currLatitudeDisplay;
	private TextView currLongitudeDisplay;
	private RadioGroup locationRadioGroup;
	private NetworkImageView currIcon;
	private View settingsView; 
	private EditText userIdTextView;
	private Menu topMenu;
	private LocationManager locationManager;

	private String currUserId;
	private String currLatitude;
	private String currLongitude;
	private boolean currIsDefLocSelected;
	
	private double currTemperature; 
	private boolean currIsInCelsius;
	
	//================================================================================
    // Activity life cycle and set up methods
    //================================================================================

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		queue = VolleySingleton.getInstance(this).getRequestQueue();
    	mImageLoader = VolleySingleton.getInstance(this).getImageLoader(); 
        currIcon = (NetworkImageView) findViewById(R.id.currentWeatherIcon);
    	currTempDisplay = (TextView) findViewById(R.id.currentTemperature);
    	currConditionDisplay = (TextView) findViewById(R.id.currentCondition);
    	currLocationDisplay = (TextView) findViewById(R.id.currentLocation);
    	settingsView = findViewById(R.id.settingsContainer);
    	userIdTextView = (EditText) findViewById(R.id.userId);
    	currLatitudeDisplay = (TextView) findViewById(R.id.latitude);
    	currLongitudeDisplay = (TextView) findViewById(R.id.longitude);
    	locationRadioGroup = (RadioGroup) findViewById(R.id.radiogroup_location);
    	
    	myPrefs = this.getSharedPreferences("myPrefs", MODE_PRIVATE);
    	String savedUserId = myPrefs.getString("USERID",null);
    	String savedLatitude = myPrefs.getString("LATITUDE",null);
    	String savedLongitude = myPrefs.getString("LONGITUDE",null);
    	boolean savedIsLocationDefault = myPrefs.getBoolean("ISLOCATIONDEFAULT", true);
    	
    	if (savedUserId != null){
        	currUserId = savedUserId;
    	} else {
        	currUserId = "";
    	}

    	if (savedLatitude != null)
    	{
        	currLatitude = savedLatitude;
    	} else {
        	currLatitude = getString(R.string.default_latitude);    		
    	}
    	
    	if (savedLongitude != null)
    	{
          	currLongitude = savedLongitude;
    	} else {
          	currLongitude = getString(R.string.default_longitude);
    	}
    	
    	if (savedIsLocationDefault == true)
    	{
          	currIsDefLocSelected = true;
          	locationRadioGroup.check(R.id.radio_default);
    	} else {
          	currIsDefLocSelected = false;
          	locationRadioGroup.check(R.id.radio_current);
    	}
      	
    	userIdTextView.setText(currUserId);

    	currTemperature = Double.NaN; // value not defined yet
    	currIsInCelsius = false;
      	currLatitudeDisplay.setText(currLatitude);
      	currLongitudeDisplay.setText(currLongitude);
    	
    	locationManager = (LocationManager) getSystemService(MainActivity.LOCATION_SERVICE);
        
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER, 
                MINIMUM_TIME_BETWEEN_UPDATES, 
                MINIMUM_DISTANCE_CHANGE_FOR_UPDATES,
                new MyLocationListener());
        
    	updateWeather();
	}

	@Override
	public void onAttachedToWindow() {
		super.onAttachedToWindow();
		((LinearLayout)findViewById(R.id.content_container)).setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(event.getAction() == MotionEvent.ACTION_DOWN) {
					updateWeather();
					return true;
				}
				return false;
			}
		});
	}
	
	 @Override
	    protected void onResume() {
	    	super.onResume();
	        imageView = (ImageView) findViewById(R.id.imageView);
	        imageView2 = (ImageView) findViewById(R.id.imageView2);
	        imageView3 = (ImageView) findViewById(R.id.imageView3);

			AnimationSet animSet = new AnimationSet(true);
			animSet.setInterpolator(new LinearInterpolator());
			ScaleAnimation zoomIn = new ScaleAnimation(1.0f, 1.5f, 1.0f, 1.5f);
			zoomIn.setDuration(20000);
			zoomIn.setRepeatMode(AnimationSet.RESTART);
			zoomIn.setRepeatCount(AnimationSet.INFINITE);
			animSet.addAnimation(zoomIn);
			AlphaAnimation alpha = new AlphaAnimation(0.5f,0.0f);
			alpha.setDuration(20000);
			animSet.addAnimation(alpha);
			alpha.setRepeatMode(AnimationSet.RESTART);
			alpha.setRepeatCount(AnimationSet.INFINITE);
			imageView.setAnimation(animSet);
			animSet.start();
			
			AnimationSet animSet2 = new AnimationSet(true);
			animSet2.setInterpolator(new LinearInterpolator());
			ScaleAnimation zoomIn2 = new ScaleAnimation(1.3f, 1.7f, 1.3f, 1.7f);
			zoomIn2.setDuration(20000);
			zoomIn2.setStartOffset(10000);
			zoomIn2.setRepeatMode(AnimationSet.RESTART);
			zoomIn2.setRepeatCount(AnimationSet.INFINITE);
			animSet2.addAnimation(zoomIn2);
			AlphaAnimation alpha2 = new AlphaAnimation(0.5f,0.0f);
			alpha2.setDuration(20000);
			alpha2.setStartOffset(10000);
			animSet2.addAnimation(alpha2);
			alpha2.setRepeatMode(AnimationSet.RESTART);
			alpha2.setRepeatCount(AnimationSet.INFINITE);
			imageView2.setAnimation(animSet2);
			animSet2.start();
			
			AnimationSet animSet3 = new AnimationSet(true);
			animSet3.setInterpolator(new LinearInterpolator());
			ScaleAnimation zoomIn3 = new ScaleAnimation(1.5f, 2.0f, 1.5f, 2.0f);
			zoomIn3.setDuration(20000);
			zoomIn2.setStartOffset(15000);
			zoomIn3.setRepeatMode(AnimationSet.RESTART);
			zoomIn3.setRepeatCount(AnimationSet.INFINITE);
			animSet3.addAnimation(zoomIn3);
			AlphaAnimation alpha3 = new AlphaAnimation(0.5f,0.0f);
			alpha3.setDuration(20000);
			alpha3.setStartOffset(15000);

			alpha3.setRepeatMode(AnimationSet.RESTART);
			alpha3.setRepeatCount(AnimationSet.INFINITE);
			animSet3.addAnimation(alpha3);
			imageView3.setAnimation(animSet3);
			animSet3.start();
	    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		topMenu = menu;
		return true;
	}
	
	//================================================================================
    // Weather update methods
    //================================================================================
	private  void parseJSON(JSONObject json) throws JSONException{ 
        JSONObject platformResponse = json.getJSONObject("platformResponse");
        JSONObject weather = platformResponse.getJSONObject("weather");
        JSONObject currentCondition = weather.getJSONObject("currentCondition");
        
        // get current location
        JSONObject locationData = currentCondition.getJSONObject("locationData");
    	currLocationDisplay.setText(locationData.getString("city"));
        
		// get current weather icon
        JSONArray providerImages = currentCondition.getJSONArray("providerImages");
        JSONObject imgObject = providerImages.getJSONObject(1);
        String imgUrl = imgObject.getString("url");
        currIcon.setImageUrl(imgUrl,mImageLoader);
        
		// get current temperature
        JSONObject weatherData = currentCondition.getJSONObject("weatherData");
        JSONObject temperatureData = weatherData.getJSONObject("temperatureData");
        String currTemp = temperatureData.getString("tempFahrenheit");
        currTemperature = Double.valueOf(currTemp);
        
        
        // check what the temperature units are
        if (currIsInCelsius == true) {
			currTemperature = convertFromFtoC();
			currTempDisplay.setText((int)currTemperature + SYMBOL_CELSIUS);
		} else {
			currTempDisplay.setText((int)currTemperature+ SYMBOL_FAHRENHEIT);
		}
        
    	
        // get current condition
    	currConditionDisplay.setText(currentCondition.getString("currCondition"));
	}
		 
	public void updateWeather() {
	    // Do something in response to button
		 ConnectivityManager connMgr = (ConnectivityManager) getSystemService(MainActivity.CONNECTIVITY_SERVICE);
		 NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			    if (networkInfo != null && networkInfo.isConnected()) {
			    	// fetch data
			    	if (currUserId == null || currUserId.equals("")) {
						currUserId = "test-1234";
					    myPrefs.edit().putString("USERID", currUserId).commit();
						Log.w("No user id", "Use default");
			    	}
					
			    	String url = "http://ec2-54-198-118-231.compute-1.amazonaws.com:8080/wunderground/getWeather?input=%7B%22gps%22:%7B%22latitude%22:"+currLatitude+",%22longitude%22:"+currLongitude+",%22radius%22:0.0%7D%7D&oauthToken=ANDROIDINTERVIEW&requestInfo=%7B%22userId%22:%22"+currUserId+"%22%7D";

			    	JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
			            @Override
			            public void onResponse(JSONObject response) {
		                	hideLoadingSpinner();
			                try {
								parseJSON(response);
							} catch (JSONException e) {
								Log.w("JSON object request", "Fail!!!");
								e.printStackTrace();
						        // display error
								AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
						        builder1.setMessage("Uh oh. Something went wrong. Try again.");
						        builder1.setCancelable(true);
						        builder1.setPositiveButton("Okay",
						                new DialogInterface.OnClickListener() {
						            public void onClick(DialogInterface dialog, int id) {
						                dialog.cancel();
						            }
						        });
						
						        AlertDialog alert11 = builder1.create();
						        alert11.show();
							}
			            }
			        }, new Response.ErrorListener() {
			 
			            @Override
			            public void onErrorResponse(VolleyError error) {
							Log.w("VolleyError", "Fail!!!");
		                	hideLoadingSpinner();
					        // display error
							AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
					        builder1.setMessage("Uh oh. Something went wrong. Try again.");
					        builder1.setCancelable(true);
					        builder1.setPositiveButton("Okay",
					                new DialogInterface.OnClickListener() {
					            public void onClick(DialogInterface dialog, int id) {
					                dialog.cancel();
					            }
					        });
					
					        AlertDialog alert11 = builder1.create();
					        alert11.show();
			            }
			        });
			 
			        queue.add(jsObjRequest);
			        showLoadingSpinner();
					
			    } else {
			        // display error
					AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
			        builder1.setMessage("No internet connection available.");
			        builder1.setCancelable(true);
			        builder1.setPositiveButton("Okay",
			                new DialogInterface.OnClickListener() {
			            public void onClick(DialogInterface dialog, int id) {
			                dialog.cancel();
			            }
			        });
			
			        AlertDialog alert11 = builder1.create();
			        alert11.show();
			    }
	}
	
	//================================================================================
    // Activity spinner methods
    //================================================================================
	private void showLoadingSpinner() {
		runOnUiThread(new Runnable() {
			public void run() {
				View loading = findViewById(R.id.loading_spinner);
				if(loading != null) {
					loading.setVisibility(View.VISIBLE);
				}
			}
		});
	}
	
	private void hideLoadingSpinner() {
		runOnUiThread(new Runnable() {
			public void run() {
				View loading = findViewById(R.id.loading_spinner);
				if(loading != null) {
					loading.setVisibility(View.GONE);
				}
			}
		});
	}
	
	//================================================================================
    // Settings methods
    //================================================================================

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			displaySettings();;
		} else if(id == R.id.action_refresh) {
			updateWeather();
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void displaySettings() {
		topMenu.setGroupVisible(R.id.group_menu, false);
		settingsView.setVisibility(View.VISIBLE);
	}
	
	public void closeSettings(View v) {
		// update user id
		currUserId = userIdTextView.getText().toString();
	    myPrefs.edit().putString("USERID", currUserId).commit();
	    myPrefs.edit().putString("LATITUDE", currLatitude).commit();
	    myPrefs.edit().putString("LONGITUDE", currLongitude).commit();
	    myPrefs.edit().putBoolean("ISLOCATIONDEFAULT", currIsDefLocSelected).commit();

		// fetch weather even if no changes in settings - better than user pressing refresh
		// TODO refine so that method is called only if there are changes in location
		updateWeather();
		
		
		// dismiss keyboard if on screen
		InputMethodManager imm = (InputMethodManager)getSystemService(MainActivity.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(userIdTextView.getWindowToken(), 0);
		// remove keyboard
		settingsView.setVisibility(View.GONE);	
		topMenu.setGroupVisible(R.id.group_menu, true);
	}
	
	private double convertFromCtoF() {
		return 9*currTemperature/5 + 32;
	}
	
	private double convertFromFtoC() {
		return (currTemperature - 32)*5/9;
	}
	
	public void onTempRadioButtonClicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.radio_celsius:
	            if (checked){
	            	if (currIsInCelsius == true) {
						// do nothing
					} else {
						currIsInCelsius = true;
						if (Double.isNaN(currTemperature) == false) {
							currTemperature = convertFromFtoC();
							currTempDisplay.setText((int)currTemperature+SYMBOL_CELSIUS);
						}
					}
	            }
	            break;
	        case R.id.radio_fahrenheit:
	            if (checked){
	            	if (currIsInCelsius == false) {
						// do nothing
					} else {
						currIsInCelsius = false;
						if (Double.isNaN(currTemperature) == false) {
							currTemperature = convertFromCtoF();
							currTempDisplay.setText((int)currTemperature+SYMBOL_FAHRENHEIT);
						}
					}
	            }
	            break;
	    }    
	}
	
	public void onLocationRadioButtonClicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.radio_default:
	            if (checked){
	            	currIsDefLocSelected = true;
	            	currLatitude = getString(R.string.default_latitude);
	            	currLongitude = getString(R.string.default_longitude);
	            	
	            	currLatitudeDisplay.setText(currLatitude);
	            	currLongitudeDisplay.setText(currLongitude);
	            }
	            break;
	        case R.id.radio_current:
	            if (checked){
	            	currIsDefLocSelected = false;
	            	 Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

	                 if (location != null) {
	 	            	currLatitude = String.valueOf(location.getLatitude());
		            	currLongitude = String.valueOf(location.getLongitude());
		            	
		            	currLatitudeDisplay.setText(String.valueOf(currLatitude));
		            	currLongitudeDisplay.setText(String.valueOf(currLongitude));
	                 } else {
	                	currLatitude = getString(R.string.default_latitude);
	 	            	currLongitude = getString(R.string.default_longitude);
	 	            	
	 	            	currLatitudeDisplay.setText(currLatitude);
	 	            	currLongitudeDisplay.setText(currLongitude);
	                 }
	            }
	            break;
	    }
	}
	
	//================================================================================
    // Location retrieval methods
    //================================================================================
	private class MyLocationListener implements LocationListener {

        public void onLocationChanged(Location location) {
        	// only update location text view if current location is selected
        	if (currIsDefLocSelected == false) {
            	currLatitude = String.valueOf(location.getLatitude());
            	currLongitude = String.valueOf(location.getLongitude());
        		currLatitudeDisplay.setText(currLatitude);
	            currLongitudeDisplay.setText(currLongitude);
	    	    myPrefs.edit().putString("LATITUDE", currLatitude).commit();
	    	    myPrefs.edit().putString("LONGITUDE", currLongitude).commit();
	            updateWeather();
			}
        	        	
            String message = String.format(
                    "New Location \n Longitude: %1$s \n Latitude: %2$s",
                    location.getLongitude(), location.getLatitude()
            );
//            Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
        }

        public void onStatusChanged(String s, int i, Bundle b) {
//            Toast.makeText(MainActivity.this, "Provider status changed",
//                    Toast.LENGTH_LONG).show();
        }

        public void onProviderDisabled(String s) {
//            Toast.makeText(MainActivity.this,
//                    "Provider disabled by the user. GPS turned off",
//                    Toast.LENGTH_LONG).show();
        }

        public void onProviderEnabled(String s) {
//            Toast.makeText(MainActivity.this,
//                    "Provider enabled by the user. GPS turned on",
//                    Toast.LENGTH_LONG).show();
        }

    }
}